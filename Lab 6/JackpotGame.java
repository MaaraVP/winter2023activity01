public class JackpotGame{
	public static void main(String[] args){
		System.out.println("Welcome to Jackpot Game!");

		Board gameBoard = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver != true){
			
			System.out.println();
			
			System.out.println(gameBoard);
			
			if(gameBoard.playATurn() == true){
				gameOver = true;
			}
			
			else{
				numOfTilesClosed++;
			}
			
		}
				
		System.out.println();
		
		System.out.println(numOfTilesClosed);
		
		if(numOfTilesClosed >= 7){
			System.out.println("You have reached the jackpot! You win!!");
		}
		else{
			System.out.println("You lost the game! :(");
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("LOSER!!!");
		}
		
	}
}
