public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		tiles = new boolean[12];
	}
	
	public String toString(){
		String boardS = "";
		for(int i = 0; i < tiles.length; i++){
			if(tiles[i] == true){
				boardS = boardS + "X ";
			}
			else{
				boardS = boardS + (i+1) + " ";
			}
		}
		return boardS;
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println(die1.toString());
		System.out.println(die2.toString());
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		if(tiles[sumOfDice - 1] == false){
			
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " +sumOfDice);
			return false;
			
		}
		
		else if(tiles[sumOfDice - 1] != false){
			
			if(tiles[die1.getFaceValue() - 1] == false){
				
				tiles[die1.getFaceValue() - 1] = true;
				System.out.println("Closing tile with the same value as die one: " +die1.getFaceValue());
				return false;
				
			}
			
			else if(tiles[die2.getFaceValue() - 1] == false){
				
				tiles[die2.getFaceValue() - 1] = true;
				System.out.println("Closing tile with the same value as die two: " +die2.getFaceValue());
				return false;
				
			}
		
			else{
			
				System.out.println("All the tiles for these values are already shut");
				return true;
			
			}
		}
		
		return true;
		
	}
	
}
