import java.util.Random;
public class Die{
	private int faceValue;
	private Random generator;
	
	public Die(){
		this.faceValue = 1;
		generator = new Random();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public int roll(){
		this.faceValue = generator.nextInt(6) + 1;
		return this.faceValue;
	}
	
	public String toString(){
		return "Rolled value is " +this.faceValue;
	}
}
