public enum Square{
	
	X,
	O,
	BLANK;
	
	public String toString(){
		
		if(this == Square.X){
			return "X";
		}
		
		else if(this == Square.O){
			return "O";
		}
		
		else if(this == Square.BLANK){
			return "_";
		}
		
		return "";
	
	}
	
}