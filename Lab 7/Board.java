public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		tictactoeBoard = new Square[3][3];
		
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
		
	}
	
	public String toString(){
		String printingBoard = "";
		for(int i = 0; i < tictactoeBoard.length; i++){
			printingBoard = printingBoard + i + "  ";
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				printingBoard = printingBoard + tictactoeBoard[i][j] + " ";
			}
			printingBoard = printingBoard + "\n";
		}
		
		return printingBoard = printingBoard + "   0 1 2";
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(row > tictactoeBoard.length || row < 0){
			System.out.println("Row out of bounds");
			return false;
		}
		else if(col > tictactoeBoard.length || col < 0){
			System.out.println("Column out of bounds");
			return false;
		}
		else{
			if(tictactoeBoard[row][col] == Square.BLANK){
				tictactoeBoard[row][col] = playerToken;
				return true;
			}
			else{
				System.out.println("Spot already taken");
				return false;
			}
		}
		
	}
	
	public boolean checkIfFull(){
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				
				if(tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}		
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0; i < tictactoeBoard.length; i++){
			
				if(tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken){
					return true;
				}	
			
		}
		
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
			for(int i = 0; i < tictactoeBoard.length; i++){
				
				if(tictactoeBoard[0][i] == playerToken && tictactoeBoard[1][i] == playerToken && tictactoeBoard[2][i] == playerToken){
					return true;
				}	
	
			}
		
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken){
		
		if(tictactoeBoard[0][0] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][2] == playerToken){
			return true;
		}
		else if(tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][0] == playerToken){
			return true;
		}
	
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		boolean isWinning = false;
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
			isWinning = true;
		}
		else{
			isWinning = false;
		}
		
		return isWinning;
	}
	
}