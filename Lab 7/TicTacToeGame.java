import java.util.Scanner;
public class TicTacToeGame{
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Welcome to the TicTacToe Game!");
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		while(gameOver != true){
			System.out.println(board.toString());
			
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			
			System.out.println("Is player " +player+ " turn. Chose your position (row).");
			int row = scan.nextInt();
			
			System.out.println("Is player " +player+ " turn. Chose your position (column).");
			int column = scan.nextInt();
			
			while(board.placeToken(row, column, playerToken) == false){
				System.out.println("Enter correct valuse please.");
				
				System.out.println("Is player " +player+ " turn. Chose your position (row).");
				row = scan.nextInt();
				
				System.out.println("Is player " +player+ " turn. Chose your position (column).");
				column = scan.nextInt();
			}
			
			if(board.checkIfFull()){
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else{ 
				if(board.checkIfWinning(playerToken)){
					System.out.println("Player " +player+ " wins!");
					gameOver = true;
				}
				else{
					player += 1;
					if(player > 2){
						player = 1;
					}
				}
			}
			
		}
		
		// Square x = Square.X;
		// Square o = Square.O;
		// Square blank = Square.BLANK;
		// System.out.println(x);
		// System.out.println(o);
		// System.out.println(blank);
	
		// Board board = new Board();
		// System.out.println(board.toString());
	}
	
}