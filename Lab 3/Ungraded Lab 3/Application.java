public class Application{
	public static void main(String[] args){
		Cat catAnimal1 = new Cat();
		catAnimal1.name = "Thomas";
		catAnimal1.food = "wet food";
		catAnimal1.toys = "mouse toy";
		
		catAnimal1.catPlays();
		catAnimal1.catEats();
		
		System.out.println();
		
		Cat catAnimal2 = new Cat();
		catAnimal2.name = "Haku";
		catAnimal2.food = "dry food";
		catAnimal2.toys = "laser toy";
		
		catAnimal2.catPlays();
		catAnimal2.catEats();
		
		System.out.println();
		
		Cat catAnimal3 = new Cat();
		catAnimal3.name = "Berry";
		catAnimal3.food = "dry food";
		catAnimal3.toys = "bird toy";
		
		catAnimal3.catPlays();
		catAnimal3.catEats();
		
		System.out.println();
		
		Cat[] clowder = new Cat[3]; 
		clowder[0] = catAnimal1;
		clowder[1] = catAnimal2;
		clowder[2] = catAnimal3;
		
		System.out.println(clowder[0].name);
		System.out.println(clowder[1].name);
		System.out.println(clowder[2].name);
		
	}
}