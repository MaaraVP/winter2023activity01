public class Refrigerator{
	public String brand;
	public double price;
	public int warrantyPeriod;
	
	public void printBrandOfRefrigerator(){
		System.out.println("You have a " +brand+ " refrigerator that costed " +price+ " Canadian dollars.");
	}
	
	public void printWarrantyPeriodForRefrigerator(){
		System.out.println("Your refrigerator has " +warrantyPeriod+ " years warranty.");
	}
}
