import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		Refrigerator[] refrigeratorList = new Refrigerator[4];
		for(int i = 0; i < refrigeratorList.length; i++){
			refrigeratorList[i] = new Refrigerator();
			
			System.out.println("What is the brand of your refrigerator?");
			refrigeratorList[i].brand = scan.next(); 
			
			System.out.println("How much did you pay for your refrigerator? (In CAD please!)");
			refrigeratorList[i].price = scan.nextDouble();
			
			System.out.println("For how many years is your warranty available?");
			refrigeratorList[i].warrantyPeriod = scan.nextInt();
			
			System.out.println();
		}
		
		System.out.println();
		
		System.out.println(refrigeratorList[3].brand+ ", " +refrigeratorList[3].price+ ", " +refrigeratorList[3].warrantyPeriod);
	
		System.out.println();
	
		refrigeratorList[0].printBrandOfRefrigerator();
		refrigeratorList[0].printWarrantyPeriodForRefrigerator();
		
	}
}
