public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		
		System.out.println(x);		
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		
		methodTwoInputNoReturn(3, 3.5);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double sumOfTwoNums = sumSquareRoot(6, 3);
		System.out.println(sumOfTwoNums);
		
		String s1 = "hello";
		String s2 = "goodbye";
		
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		
		System.out.println(sc.addOne(50));
		System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input");
		int x = 50;
		System.out.println(x);
	}
	
	public static int methodOneInputNoReturn(int value){
		System.out.println("Inside the method one input no return");
		System.out.println(value);
		
		return value;
	}
	
	public static void methodTwoInputNoReturn(int value2, double value3){
		
	}
	
	public static int methodNoInputReturnInt(){
		int alwaysSameValue = 6;
		return alwaysSameValue;
	}
	
	public static double sumSquareRoot(int a, int b){
		double sum = a + b;
		sum = Math.sqrt(sum);
		return sum;
	}
}
