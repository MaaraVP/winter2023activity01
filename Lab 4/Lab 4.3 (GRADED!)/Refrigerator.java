public class Refrigerator{
	private String brand;
	private double price;
	private int warrantyPeriod;
	
	public void printBrandOfRefrigerator(){
		System.out.println("You have a " +brand+ " refrigerator that costed " +price+ " Canadian dollars.");
	}
	
	public void printWarrantyPeriodForRefrigerator(){
		System.out.println("Your refrigerator has " +warrantyPeriod+ " years warranty.");
	}
	
	//Lab 4 Part 1
	public void sellRefrigerator(String userAnswer){
		if(userAnswer.equals("yes") || userAnswer.equals("Yes")){
			System.out.println("You are selling your " +brand+ " refrigerator for " +this.price+ " Canadian dollars!");
		}
		else{
			System.out.println("You are not selling your " +brand+ " refrigerator!");
		}
	}
	
	//Part 2
	//Getters
	public String getBrand(){
		return this.brand;
	}
	public double getPrice(){
		return this.price;
	}
	public int getWarrantyPeriod(){
		return this.warrantyPeriod;
	}
	//Setters
	public void setBrand(String newBrand){
		this.brand = newBrand;
	}
	public void setPrice(double newPrice){
		this.price = newPrice;
	}
	public void setWarrantyPeriod(int newWarrantyPeriod){
		this.warrantyPeriod = newWarrantyPeriod;
	}
	
	
}
