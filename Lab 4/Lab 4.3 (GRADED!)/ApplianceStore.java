import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		Refrigerator[] refrigeratorList = new Refrigerator[4];
		
		for(int i = 0; i < refrigeratorList.length; i++){
			refrigeratorList[i] = new Refrigerator();
			
			System.out.println("What is the brand of your refrigerator?");
			String brand = scan.next();
			refrigeratorList[i].setBrand(brand); 
			
			System.out.println("How much did you pay for your refrigerator? (In CAD please!)");
			double price = scan.nextDouble();
			refrigeratorList[i].setPrice(price);
			
			System.out.println("For how many years is your warranty available?");
			int warrantyPeriod = scan.nextInt();
			refrigeratorList[i].setWarrantyPeriod(warrantyPeriod);
			
			System.out.println();
		}
		
		System.out.println();
		
		
		System.out.println(refrigeratorList[3].getBrand()+ ", " +refrigeratorList[3].getPrice()+ ", " +refrigeratorList[3].getWarrantyPeriod());
	
		System.out.println();
	
		refrigeratorList[0].printBrandOfRefrigerator();
		refrigeratorList[0].printWarrantyPeriodForRefrigerator();
		
		System.out.println();
		
		//Lab 4 Part 1
		System.out.println("Would you like to sell your refrigerator?");
		String userAnswer = scan.next();
		if(userAnswer.equals("yes") || userAnswer.equals("Yes")){
			System.out.println("For how much would you like to sell it?");
			double price = scan.nextDouble();
			refrigeratorList[1].setPrice(price);
			refrigeratorList[1].sellRefrigerator(userAnswer);
		}
		else{
			refrigeratorList[1].sellRefrigerator(userAnswer);
		}
		
	}
}
