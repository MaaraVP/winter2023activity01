public class Student{
	//Fields of the class Student
	private String name;
	private String program;
	private int grade;

	//Part 1 (Question 1)
	public int amountLearnt;
	
	//The 2 instance methods that represent actions of the Student object
	public void sayHi(){
		System.out.println("The student's is " +name);
	}
	
	public void examGrade(){
		System.out.println(name+ " is in " +program+ " program");
	}
	
	//Part 1 (Question 4) and Part 2 (Question 1)
	public void learn(int amountStudied){
		if(amountStudied > 0){
			amountLearnt = amountLearnt + amountStudied;
		}
	}
	
	//Part 3 (Question 2)
	public String getName(){
		return this.name;
	}
	public String getProgram(){
		return this.program;
	}
	public int getGrade(){
		return this.grade;
	}
	
	//Part 3 (Question 5)
	public void setName(String newName){
		this.name = newName;
	}
	public void setProgram(String newProgram){
		this.program = newProgram;
	}
	public void setGrade(int newGrade){
		this.grade = newGrade;
	}
	
}
