import java.util.Scanner;
public class Application{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);

		Student[] studentsSection = new Student[4];
		
		for(int i = 0; i < studentsSection.length; i++){
			studentsSection[i] = new Student();
			
			System.out.println("What is your name?");
			String name = scan.next();
			studentsSection[i].setName(name);
			
			System.out.println("What program are you in?");
			String program = scan.next();
			studentsSection[i].setProgram(program);

			System.out.println("What is your grade in the most recent assignment?");
			int grade = scan.nextInt();
			studentsSection[i].setGrade(grade);
			
			System.out.println();
	
			//Part 1 (Question 2)
			System.out.println(studentsSection[i].amountLearnt);
		}
		System.out.println();
		
		//Part 2(Question 2)
		System.out.println("How many hours did you study?");
		int amountStudied = scan.nextInt();
		
		System.out.println();
		
		//Part 1 (Question 5) and Part 2 (Question 3)
		studentsSection[3].learn(amountStudied);
		studentsSection[3].learn(amountStudied);
		studentsSection[3].learn(amountStudied);
		
		System.out.println();
		
		//Part 1 (Question 6)
		System.out.println(studentsSection[0].amountLearnt);
		System.out.println(studentsSection[1].amountLearnt);
		System.out.println(studentsSection[2].amountLearnt);
		System.out.println(studentsSection[3].amountLearnt);
		
		System.out.println();
		
		//Part 2.5 
		studentsSection[0].learn(-1);
		studentsSection[1].learn(1);
		System.out.println(studentsSection[0].amountLearnt);
		System.out.println(studentsSection[1].amountLearnt);
		
		System.out.println();
		
		//Part 3 (Question 2, Question 8 and Question 9)
	
		System.out.println(studentsSection[0].getName());
		System.out.println(studentsSection[0].getProgram());
		System.out.println(studentsSection[0].getGrade());
		System.out.println(studentsSection[0].amountLearnt);
		
		System.out.println();
		
		studentsSection[0].setName("Maara");
		studentsSection[0].setProgram("CS");
		studentsSection[0].setGrade(100);
		
		System.out.println();
		
		System.out.println(studentsSection[0].getName());
		System.out.println(studentsSection[0].getProgram());
		System.out.println(studentsSection[0].getGrade());
		
	}
}
