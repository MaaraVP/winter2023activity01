public class Student{
	//Fields of the class Student
	private String name;
	private String program;
	private int grade;

	//Lab 4.1
	public int amountLearnt;
	
	//Lab 4.1
	public void sayHi(){
		System.out.println("The student's is " +name);
	}
	
	public void examGrade(){
		System.out.println(name+ " is in " +program+ " program");
	}
	
	//Lab 4.1
	public void learn(int amountStudied){
		if(amountStudied > 0){
			amountLearnt = amountLearnt + amountStudied;
		}
	}
	
	//Lab 4.1
	public String getName(){
		return this.name;
	}
	public String getProgram(){
		return this.program;
	}
	public int getGrade(){
		return this.grade;
	}
	
	//Lab 4.1
	public void setName(String newName){
		this.name = newName;
	}
	public void setProgram(String newProgram){
		this.program = newProgram;
	}
	public void setGrade(int newGrade){
		this.grade = newGrade;
	}
	
	//Lab 4.2 (Question 1)
	public Student(String name, String program){
		this.name = name;
		this.program = program;
		this.grade = 85;
		this.amountLearnt = 0;
	}
	
}
